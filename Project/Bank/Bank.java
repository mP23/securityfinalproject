package Project.Bank;
import java.net.*;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.nio.charset.StandardCharsets;
import javax.crypto.*;
import java.security.*;
import java.security.spec.*;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.xml.bind.DatatypeConverter;

public class Bank {
    public static void main(String[] args) throws Exception {
        if(args.length != 1){
            System.out.println("Invalid number of arguments!");
            System.out.println("Usage: $java Bank <bank-port>");
            System.exit(1);
        }
        ServerSocket listen = new ServerSocket(Integer.parseInt(args[0]));
        Socket conn = listen.accept();
        System.out.println("Connected");

	while(true) {
        BufferedReader fromPServ = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        //toPurchasingServer
        PrintWriter toPServ = new PrintWriter(conn.getOutputStream(), true);

        //Read E(Pub, <name || credit card number>)
        String encryptedIn = fromPServ.readLine();

        //Decrypt using Bank's private key
        PrivateKey privKey = readPrivateKeyFromFile("Project/Bank/private.key");
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privKey);
        byte[] decryptedInfo = cipher.doFinal(DatatypeConverter.parseBase64Binary(encryptedIn));
        String decrypted = new String(decryptedInfo, "UTF-8");
        //System.out.println("Decrypted name and number is " + decrypted);
        StringTokenizer tok = new StringTokenizer(decrypted, " ");
        String nameIn = tok.nextToken();
        String creditCardNum = tok.nextToken();

        HashMap<String, String> cardNums = new HashMap<String, String>();
        HashMap<String, Integer> balances = new HashMap<String, Integer>();
        ArrayList<String> names = new ArrayList<String>();

        //Read from file
        BufferedReader fileIn = new BufferedReader(new FileReader("Project/Bank/balance"));
        String line;
        while((line = fileIn.readLine()) != null){
            StringTokenizer lineTok = new StringTokenizer(line, ", ");
            String name = lineTok.nextToken();
            String cardNum = lineTok.nextToken();
            Integer balance = Integer.parseInt(lineTok.nextToken());
            names.add(name);
            cardNums.put(name, cardNum);
            balances.put(name, balance);
        } 
        
        //Compare credit card number
        String creditCardCurr = cardNums.get(nameIn);
            //If no match, then send "error"
        if(creditCardCurr == null){
            toPServ.println("error");
            fileIn.close();
        } else {
            //Else, decrypt E(Prp, price) using Pup
                encryptedIn = fromPServ.readLine();
                PublicKey pup = readPublicKeyFromFile("Project/Psystem/public.key");
                cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                cipher.init(Cipher.DECRYPT_MODE, pup);
                decrypted = new String(cipher.doFinal(DatatypeConverter.parseBase64Binary(encryptedIn)), "UTF-8");
                int price = Integer.parseInt(decrypted);
                //Update credit card balance
                balances.put(nameIn, price + balances.get(nameIn));
                //Send "ok" to purchasing server
                toPServ.println("ok");
                fileIn.close();

                //Update the information in balances file
                FileOutputStream fileOutStream = new FileOutputStream("Project/Bank/balance");
                BufferedWriter fileOut = new BufferedWriter(new OutputStreamWriter(fileOutStream));
                for(int i = 0; i < names.size(); i++){
                    line = "" + names.get(i) + ", " + cardNums.get(names.get(i)) + ", " + Integer.toString(balances.get(names.get(i))) + "\n";
                    fileOut.write(line);
                }
                fileOut.close();
        }
        
        fromPServ.close();
        toPServ.close();
        conn.close();
        break;    
        }
    }

    public static PublicKey readPublicKeyFromFile(String keyFileName) throws IOException{
        FileInputStream in = new FileInputStream(keyFileName);
        ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));
        try {
            BigInteger m = (BigInteger) oin.readObject();
            BigInteger e = (BigInteger) oin.readObject();
            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e);
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PublicKey pubKey = fact.generatePublic(keySpec);
            return pubKey;
        } catch (Exception e){
            throw new RuntimeException("Spurious serialisation error", e);
        } finally {
            oin.close();
        }
    }



    public static PrivateKey readPrivateKeyFromFile(String keyFileName) throws IOException {
        FileInputStream in = new FileInputStream(keyFileName);
        ObjectInputStream oin =
            new ObjectInputStream(new BufferedInputStream(in));
        try {
            BigInteger m = (BigInteger) oin.readObject();
            BigInteger e = (BigInteger) oin.readObject();
            RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(m, e);
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PrivateKey privKey = fact.generatePrivate(keySpec);
            return privKey;
        } catch (Exception e) {
            throw new RuntimeException("Spurious serialisation error", e);
        } finally {
            oin.close();
        }
    }

}


