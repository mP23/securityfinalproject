package Project.Customer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.net.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import javax.crypto.*;
import java.math.BigInteger;
import java.security.spec.*;
import java.security. *;
import javax.xml.bind.DatatypeConverter;

public class Customer {

    public static void main(String[] args) throws Exception {
        if(args.length != 2){
            System.out.println("Incorrect number of arguments!");
            System.out.println("Usage: $java Customer <purchasing system domain> <purchasing system port>");
            System.exit(1);
        }
        
        //Connect to purchasing server
        Socket sock = new Socket(args[0], Integer.parseInt(args[1]));
        System.out.println("Socket Connected");
        PrintWriter sockOut = new PrintWriter(sock.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));

        //Prompt user for ID and password
        System.out.print("Enter User ID: ");
        BufferedReader sysIn = new BufferedReader(new InputStreamReader(System.in));
        String userID = "";
        try {
            while((userID = sysIn.readLine()) == null){
                System.out.print("Enter User ID: ");
            }
        } catch (IOException e) {
            System.out.println("IOException was caught while trying to read ID");
            System.exit(1);
        }
        sockOut.println(userID);
        System.out.print("Enter password: ");
        String password = "";
        String reply = "";
        int incorrectPasswordFlag = 1;
        while(incorrectPasswordFlag == 1){
            try {
                while((password = sysIn.readLine()) == null){
                    System.out.print("Enter password: ");
                }
            } catch (IOException e){
                System.out.println("IOException was caught while trying to read password");
                System.exit(1);
            }
            //System.out.println("User ID: " + userID);
            //System.out.println("Password: " + password); 

            /*TEMPORARY CREATE A PASSWORD FILE FROM HASH*/
            /*PrintWriter temp = new PrintWriter("password", "UTF-8");
            temp.println("ID: alice");
            temp.println("Password: " + hash("1234"));
            temp.println("ID: tom");
            temp.println("Password: " + hash("5678"));
            temp.close();
            System.out.println("Done hashing passwords");
            */
         

            //Perform hash on password    
            String hashedPass = hash(password);
            sockOut.println(hashedPass);

            reply = in.readLine();
            if(!(reply.trim()).equals("error")){
                incorrectPasswordFlag = 0;
            } else {
                System.out.print("Enter password: ");
            }

        }
        //Password is correct; print the list out
        while(!reply.equals("done")){
            System.out.println(reply);
            reply = in.readLine();
        }

        PublicKey pubKey = readKeyFromFile("Project/Psystem/public.key");

        System.out.print("Please enter the item #: ");
        String itemNum = sysIn.readLine();

        System.out.print("Please enter credit card #: ");
        String creditCardNum = userID + " " + sysIn.readLine();

        //System.out.println("encrypting item # " + itemNum);
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        byte[] encryptedItemNum = cipher.doFinal(itemNum.getBytes("UTF-8"));

        //System.out.println("Sending: " + Integer.toString(encryptedItemNum.length));
        //sockOut.println(Integer.toString(encryptedItemNum.length));
        sockOut.println(DatatypeConverter.printBase64Binary(encryptedItemNum));

        //OutputStream sockOutStream = sock.getOutputStream();
        //sockOutStream.write(encryptedItemNum);


        //Send E(pup, item num)
        //sockOut.println(new String(encryptedItemNum, StandardCharsets.UTF_8));
        //sockOut.println(Base64.getUrlEncoder().encodeToString(encryptedItemNum));

        //Create Digital Signature
        String privateKeyFile = "Project/Customer/" + userID + "Private.key";
        PrivateKey userPrivKey = readPrivateKeyFromFile(privateKeyFile); 
        Signature ds = Signature.getInstance("SHA1withRSA");
        ds.initSign(userPrivKey);
        ds.update(DatatypeConverter.parseBase64Binary(itemNum), 0, DatatypeConverter.parseBase64Binary(itemNum).length);
        byte[] realSig = ds.sign();
        //Send digital signature
        String sigOut = DatatypeConverter.printBase64Binary(realSig);
        //System.out.println("Sending: " + sigOut);
        sockOut.println(sigOut);

        //Send E(pub, <name || credit card number>
        PublicKey pubBankKey = readKeyFromFile("Project/Bank/public.key");
        cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, pubBankKey);
        byte[] encryptedCreditCardNum = cipher.doFinal(creditCardNum.getBytes("UTF-8"));
        sockOut.println(DatatypeConverter.printBase64Binary(encryptedCreditCardNum));

        reply = in.readLine();
        if(reply.equals("ok")){
            System.out.println("we will process your order soon");
        } else {
            System.out.println("wrong credit card number");
        }
    }

    public static String hash(String strin){
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA1");
        } catch (NoSuchAlgorithmException e){
            System.out.println("No algorithm called SHA1 was found!");
            return null;
        }
        byte[] result = digest.digest(strin.getBytes());
        return new String(result, StandardCharsets.UTF_8); 
    }

    public static PublicKey readKeyFromFile(String keyFileName) throws IOException {
        FileInputStream in = new FileInputStream(keyFileName);
        ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));
          try {
              BigInteger m = (BigInteger) oin.readObject();
              BigInteger e = (BigInteger) oin.readObject();
              RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e);
              KeyFactory fact = KeyFactory.getInstance("RSA");
              PublicKey pubKey = fact.generatePublic(keySpec);
              return pubKey;
          } catch (Exception e) {
              throw new RuntimeException("Spurious serialisation error", e);
          } finally {
              oin.close();
          }
    }
    
    public static PrivateKey readPrivateKeyFromFile(String keyFileName) throws IOException {
        FileInputStream in = new FileInputStream(keyFileName);
        ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));
          try {
              BigInteger m = (BigInteger) oin.readObject();
              BigInteger e = (BigInteger) oin.readObject();
              RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(m, e);
              KeyFactory fact = KeyFactory.getInstance("RSA");
              PrivateKey privKey = fact.generatePrivate(keySpec);
              return privKey;
          } catch (Exception e) {
              throw new RuntimeException("Spurious serialisation error", e);
          } finally {
              oin.close();
          }
    }
}
